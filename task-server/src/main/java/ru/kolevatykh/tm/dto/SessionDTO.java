package ru.kolevatykh.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.SessionNotFoundException;
import ru.kolevatykh.tm.exception.UserNotFoundException;
import ru.kolevatykh.tm.service.UserService;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

import java.io.IOException;

public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    private String signature;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @NotNull
    private String userId;

    @NotNull
    private RoleType roleType;

    public SessionDTO() {
    }

    public static String generateSignature(@NotNull final SessionDTO session)
            throws IOException, CloneNotSupportedException {
        @NotNull final SessionDTO tempSession = (SessionDTO) session.clone();
        tempSession.setSignature(null);
        return SignatureUtil.sign(tempSession, ConfigUtil.getSalt(), ConfigUtil.getCycle());
    }

    @NotNull
    public static Session getSessionEntity(@Nullable final SessionDTO sessionDTO)
            throws SessionNotFoundException, UserNotFoundException {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @NotNull final UserService userService = new UserService();
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        if (userService.findOneById(sessionDTO.getUserId()) != null)
            session.setUser(userService.findOneById(sessionDTO.getUserId()));
        session.setRoleType(sessionDTO.getRoleType());
        return session;
    }

    @Nullable
    public String getSignature() {
        return this.signature;
    }

    @NotNull
    public Long getTimestamp() {
        return this.timestamp;
    }

    @NotNull
    public String getUserId() {
        return this.userId;
    }

    @NotNull
    public RoleType getRoleType() {
        return this.roleType;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    public void setTimestamp(@NotNull final Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUserId(@NotNull final String userId) {
        this.userId = userId;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }
}
