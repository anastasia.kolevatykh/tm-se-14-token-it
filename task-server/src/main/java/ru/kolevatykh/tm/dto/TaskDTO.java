package ru.kolevatykh.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.UserService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PROPERTY)
public final class TaskDTO extends AbstractProjectTaskEntityDTO implements Serializable {

    @Nullable
    protected String projectId;

    public TaskDTO() {
    }

    public TaskDTO(@NotNull final String name, @NotNull final String description,
                   @Nullable final Date startDate, @Nullable final Date finishDate) {
        super(name, description, startDate, finishDate);
    }

    @NotNull
    public static Task getTaskEntity(@Nullable final TaskDTO taskDTO) throws Exception {
        if (taskDTO == null) throw new Exception("[The task does not exist.]");
        @NotNull final UserService userService = new UserService();
        @NotNull final ProjectService projectService = new ProjectService();
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setUser(userService.findOneById(taskDTO.getUserId()));
        if (taskDTO.getProjectId() != null)
            task.setProject(projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId()));
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setCreateDate(taskDTO.getCreateDate());
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setStatusType(taskDTO.getStatusType());
        return task;
    }

    @Nullable
    public String getProjectId() {
        return this.projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }
}
