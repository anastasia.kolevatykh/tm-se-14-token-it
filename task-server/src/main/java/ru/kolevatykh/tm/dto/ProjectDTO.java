package ru.kolevatykh.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.service.UserService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PROPERTY)
public final class ProjectDTO extends AbstractProjectTaskEntityDTO implements Serializable {

    public ProjectDTO() {
    }

    public ProjectDTO(@NotNull final String name, @NotNull final String description,
                      @Nullable final Date startDate, @Nullable final Date finishDate) {
        super(name, description, startDate, finishDate);
    }

    @NotNull
    public static Project getProjectEntity(@Nullable final ProjectDTO projectDTO) throws Exception {
        if (projectDTO == null) throw new Exception("[The project does not exist.]");
        @NotNull final UserService userService = new UserService();
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(userService.findOneById(projectDTO.getUserId()));
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setCreateDate(projectDTO.getCreateDate());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setStatusType(projectDTO.getStatusType());
        return project;
    }
}
