package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IProjectTaskService<Task> {

    @NotNull List<Task> findAll() throws Exception;

    @NotNull List<Task> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<Task> findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void persist(@Nullable Task task) throws Exception;

    void merge(@Nullable Task task) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;

    void removeTasksWithProjectId(@Nullable String userId) throws Exception;

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull List<Task> findTasksWithoutProject(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStartDate(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByFinishDate(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllSortedByStatus(@Nullable String userId) throws Exception;

    @NotNull List<Task> findAllBySearch(@Nullable String userId, @Nullable String search) throws Exception;
}
