package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull List<Session> findAll() throws Exception;

    @NotNull List<Session> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable Session findOneById(@NotNull String id) throws Exception;

    void persist(@NotNull Session session) throws Exception;

    void merge(@NotNull Session session) throws Exception;

    void remove(@NotNull Session session) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull String userId) throws Exception;
}
