package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    @Nullable ServiceLocator getServiceLocator();

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    void serialize() throws Exception;

    void deserialize() throws Exception;

    void saveJacksonJson() throws Exception;

    void loadJacksonJson() throws Exception;

    void saveJacksonXml() throws Exception;

    void loadJacksonXml() throws Exception;

    void marshalJaxbJson() throws Exception;

    void unmarshalJaxbJson() throws Exception;

    void marshalJaxbXml() throws Exception;

    void unmarshalJaxbXml() throws Exception;
}
