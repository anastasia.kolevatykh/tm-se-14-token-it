package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

public interface ISessionService extends IService<Session> {

    @Nullable User getUser(@Nullable SessionDTO session) throws Exception;

    @NotNull SessionDTO openAuth(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull SessionDTO openReg(@NotNull String login, @NotNull String password) throws Exception;

    void validate(@Nullable SessionDTO sessionDTO) throws Exception;

    void removeAllByUserId(@Nullable String userId) throws Exception;
}
