package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskRepository<T> {

    @NotNull List<T> findAll() throws Exception;

    @NotNull List<T> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable T findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable List<T> findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void persist(@NotNull T entity) throws Exception;

    void merge(@NotNull T entity) throws Exception;

    void remove(@NotNull T entity) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull String userId) throws Exception;

    @NotNull List<T> findAllSortedByCreateDate(@NotNull String userId) throws Exception;

    @NotNull List<T> findAllSortedByStartDate(@NotNull String userId) throws Exception;

    @NotNull List<T> findAllSortedByFinishDate(@NotNull String userId) throws Exception;

    @NotNull List<T> findAllSortedByStatus(@NotNull String userId) throws Exception;

    @NotNull List<T> findAllBySearch(@NotNull String userId, @NotNull String search) throws Exception;
}
