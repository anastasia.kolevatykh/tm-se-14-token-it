package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {

    @NotNull List<Project> findAll() throws Exception;

    @NotNull List<Project> findAllByUserId(@NotNull String userId) throws Exception;

    @Nullable Project findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull List<Project> findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void persist(@NotNull Project project) throws Exception;

    void merge(@NotNull Project project) throws Exception;

    void remove(@NotNull Project project) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllSortedByCreateDate(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStartDate(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllSortedByFinishDate(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllSortedByStatus(@NotNull String userId) throws Exception;

    @NotNull List<Project> findAllBySearch(@NotNull String userId, @NotNull String search) throws Exception;
}
