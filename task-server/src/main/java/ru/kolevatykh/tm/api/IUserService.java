package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.exception.UserNotFoundException;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable List<User> findAll() throws Exception;

    @Nullable User findOneById(@Nullable String id) throws Exception;

    @Nullable User findOneByLogin(@Nullable String login) throws UserNotFoundException;

    void persist(@Nullable User user) throws UserNotFoundException;

    void merge(@Nullable User user) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll() throws Exception;
}
