package ru.kolevatykh.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.dto.TaskDTO;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_task")
public final class Task extends AbstractProjectTaskEntity {

    @ManyToOne
    @JoinColumn(name = "project_id")
    @Nullable
    protected Project project;

    public Task() {
    }

    public Task(@NotNull final String name, @NotNull final String description,
                @Nullable final Date startDate, @Nullable final Date finishDate) {
        super(name, description, startDate, finishDate);
    }

    @NotNull
    public TaskDTO getTaskDTO(@Nullable final Task task) throws Exception {
        if (task == null) throw new Exception("[The task does not exist.]");
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        if (task.getProject() != null)
            taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setCreateDate(task.getCreateDate());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        taskDTO.setStatusType(task.getStatusType());
        return taskDTO;
    }

    @NotNull
    public List<TaskDTO> getListTaskDTO(@Nullable final List<Task> tasks) throws Exception {
        if (tasks == null) throw new Exception("[There're no tasks yet.]");
        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            listTaskDTO.add(getTaskDTO(task));
        }
        return listTaskDTO;
    }

    @Nullable
    public Project getProject() {
        return this.project;
    }

    public void setProject(@Nullable final Project project) {
        this.project = project;
    }
}
