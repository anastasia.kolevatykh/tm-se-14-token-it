package ru.kolevatykh.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.dto.ProjectDTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_project")
public final class Project extends AbstractProjectTaskEntity {

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project() {
    }

    public Project(@NotNull final String name, @NotNull final String description,
                   @Nullable final Date startDate, @Nullable final Date finishDate) {
        super(name, description, startDate, finishDate);
    }

    @NotNull
    public ProjectDTO getProjectDTO(@Nullable final Project project) throws Exception {
        if (project == null) throw new Exception("[The project does not exist.]");
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setCreateDate(project.getCreateDate());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        projectDTO.setStatusType(project.getStatusType());
        return projectDTO;
    }

    @NotNull
    public List<ProjectDTO> getListProjectDTO(@Nullable final List<Project> projects) throws Exception {
        if (projects == null) throw new Exception("[There're no projects yet.]");
        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            listProjectDTO.add(getProjectDTO(project));
        }
        return listProjectDTO;
    }

    @NotNull
    public List<Task> getTasks() {
        return this.tasks;
    }

    public void setTasks(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }
}
