package ru.kolevatykh.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

public final class Token extends AbstractEntity implements Cloneable {

    @Nullable
    private SessionDTO sessionDTO;

    @Nullable
    private String signature;

    public Token() {
    }

    public static String generateSignature(@NotNull final Token token) throws Exception {
        @NotNull final Token tempToken = (Token) token.clone();
        tempToken.setSignature(null);
        return SignatureUtil.sign(tempToken, ConfigUtil.getSalt(), ConfigUtil.getCycle());
    }

    @Nullable
    public SessionDTO getSessionDTO() {
        return this.sessionDTO;
    }

    @Nullable
    public String getSignature() {
        return this.signature;
    }

    public void setSessionDTO(@Nullable final SessionDTO sessionDTO) {
        this.sessionDTO = sessionDTO;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }
}
