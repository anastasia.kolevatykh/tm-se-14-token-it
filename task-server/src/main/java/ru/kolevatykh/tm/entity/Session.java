package ru.kolevatykh.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.SessionNotFoundException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_session")
public final class Session extends AbstractEntity implements Cloneable {

    @Basic
    @Nullable
    private String signature;

    @Basic(optional = false)
    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private RoleType roleType;

    public Session() {
    }

    @NotNull
    public SessionDTO getSessionDTO(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setRoleType(session.getRoleType());
        return sessionDTO;
    }

    @NotNull
    public List<SessionDTO> getListSessionDTO(@Nullable final List<Session> sessions) throws SessionNotFoundException {
        if (sessions == null) throw new SessionNotFoundException();
        @NotNull final List<SessionDTO> listSessionDTO = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            listSessionDTO.add(getSessionDTO(session));
        }
        return listSessionDTO;
    }

    @Nullable
    public String getSignature() {
        return this.signature;
    }

    @NotNull
    public Long getTimestamp() {
        return this.timestamp;
    }

    @NotNull
    public User getUser() {
        return this.user;
    }

    @NotNull
    public RoleType getRoleType() {
        return this.roleType;
    }

    public void setSignature(@Nullable final String signature) {
        this.signature = signature;
    }

    public void setTimestamp(@NotNull final Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUser(@NotNull final User user) {
        this.user = user;
    }

    public void setRoleType(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }
}
