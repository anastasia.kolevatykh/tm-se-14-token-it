package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.dto.UserDTO;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.CommandAdminOnlyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<UserDTO> findAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @NotNull final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        return new User().getListUserDTO(serviceLocator.getUserService().findAll());
    }

    @Override
    @Nullable
    @WebMethod
    public final UserDTO findUserById(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @NotNull final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return new User().getUserDTO(serviceLocator.getUserService().findOneById(sessionDTO.getUserId()));
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "loginNew") @NotNull final String loginNew,
            @WebParam(name = "password") @NotNull final String password,
            @WebParam(name = "role") @Nullable final String role
    ) throws Exception {
        if (serviceLocator == null) return;
        @NotNull final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);
        if (user == null) throw new Exception("[The login is not found.]");
        user.setLogin(loginNew);
        user.setPasswordHash(password);
        user.setRoleType(RoleType.valueOf(role));
        serviceLocator.getUserService().merge(user);
        serviceLocator.getSessionService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @NotNull final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final String userId = sessionDTO.getUserId();
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @NotNull final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getUserService().removeAll();
    }
}
