package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.dto.ProjectDTO;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.enumerate.StatusType;
import ru.kolevatykh.tm.exception.SessionNotFoundException;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllByUserId(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final ProjectDTO findProjectById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return new Project().getProjectDTO(serviceLocator.getProjectService().findOneById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return new Project().getListProjectDTO(serviceLocator.getProjectService().findOneByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setUserId(sessionDTO.getUserId());
        projectDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        projectDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getProjectService().persist(ProjectDTO.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final ProjectDTO projectDTO =
                new Project().getProjectDTO(serviceLocator.getProjectService().findOneById(sessionDTO.getUserId(), id));
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectDTO.setUserId(sessionDTO.getUserId());
        projectDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        projectDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getProjectService().merge(ProjectDTO.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProjectStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final ProjectDTO projectDTO =
                new Project().getProjectDTO(serviceLocator.getProjectService().findOneById(sessionDTO.getUserId(), id));
        projectDTO.setStatusType(StatusType.valueOf(status));
        serviceLocator.getProjectService().merge(ProjectDTO.getProjectEntity(projectDTO));
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getProjectService().remove(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws SessionNotFoundException, Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getProjectService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByCreateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllSortedByCreateDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllSortedByStartDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllSortedByFinishDate(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllSortedByStatus(sessionDTO.getUserId()));
        return projects;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<ProjectDTO> findProjectsBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull List<ProjectDTO> projects =
                new Project().getListProjectDTO(serviceLocator.getProjectService().findAllBySearch(sessionDTO.getUserId(), search));
        return projects;
    }
}
