package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.dto.TaskDTO;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.enumerate.StatusType;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public final List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @NotNull final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllByUserId(sessionDTO.getUserId()));
        return tasks;
    }

    @Nullable
    @WebMethod
    public final TaskDTO findTaskById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return new Task().getTaskDTO(serviceLocator.getTaskService().findOneById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTaskByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        return new Task().getListTaskDTO(serviceLocator.getTaskService().findOneByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setProjectId(projectId);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setUserId(sessionDTO.getUserId());
        taskDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        taskDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getTaskService().persist(TaskDTO.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "finishDate") @Nullable final String finishDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                new Task().getTaskDTO(serviceLocator.getTaskService().findOneById(sessionDTO.getUserId(), id));
        taskDTO.setProjectId(projectId);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setUserId(sessionDTO.getUserId());
        taskDTO.setStartDate(DateFormatterUtil.parseDate(startDate));
        taskDTO.setFinishDate(DateFormatterUtil.parseDate(finishDate));
        serviceLocator.getTaskService().merge(TaskDTO.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void mergeTaskStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                new Task().getTaskDTO(serviceLocator.getTaskService().findOneById(sessionDTO.getUserId(), id));
        taskDTO.setStatusType(StatusType.valueOf(status));
        serviceLocator.getTaskService().merge(TaskDTO.getTaskEntity(taskDTO));
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getTaskService().remove(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getTaskService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByCteateDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllSortedByCreateDate(sessionDTO.getUserId()));
        return tasks;
    }
    
    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllSortedByStartDate(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByFinishDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllSortedByFinishDate(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllSortedByStatus(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findAllBySearch(sessionDTO.getUserId(), search));
        return tasks;
    }

    @Override
    @WebMethod
    public void removeTasksWithProjectId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getTaskService().removeTasksWithProjectId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectTasks(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        serviceLocator.getTaskService().removeProjectTasks(sessionDTO.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksByProjectId(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findTasksByProjectId(sessionDTO.getUserId(), projectId));
        return tasks;
    }

    @Override
    @Nullable
    @WebMethod
    public final List<TaskDTO> findTasksWithoutProject(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return null;
        @Nullable final List<TaskDTO> tasks =
                new Task().getListTaskDTO(serviceLocator.getTaskService().findTasksWithoutProject(sessionDTO.getUserId()));
        return tasks;
    }

    @Override
    @WebMethod
    public void assignToProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        @Nullable final TaskDTO taskDTO =
                new Task().getTaskDTO(serviceLocator.getTaskService().findOneById(sessionDTO.getUserId(), id));
        taskDTO.setProjectId(projectId);
        serviceLocator.getTaskService().merge(TaskDTO.getTaskEntity(taskDTO));
    }
}
