package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;

class AbstractEndpoint {

    @Nullable
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint() {
    }

    AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
