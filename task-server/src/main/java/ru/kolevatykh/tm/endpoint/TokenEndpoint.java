package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.dto.UserDTO;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.exception.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ITokenEndpoint")
public class TokenEndpoint extends AbstractEndpoint implements ITokenEndpoint {

    public TokenEndpoint() {
    }

    public TokenEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public String openTokenSessionAuth(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws AuthenticationException, Exception, UserNotFoundException {
        try {
            if (serviceLocator == null) return null;
            if (login == null || login.isEmpty()) throw new EmptyInputException("Login");
            if (password == null || password.isEmpty()) throw new EmptyInputException("Password");
            @Nullable final String tokenString = serviceLocator.getTokenService().openAuth(login, password);
            return tokenString;
        } catch (EmptyInputException | InvalidPasswordException e) {
            throw new AuthenticationException();
        }
    }

    @Override
    @Nullable
    @WebMethod
    public String openTokenSessionReg(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws EmptyInputException, AuthenticationException, Exception {
        try {
            if (serviceLocator == null) return null;
            if (login == null || login.isEmpty()) throw new EmptyInputException("Login");
            if (password == null || password.isEmpty()) throw new EmptyInputException("Password");
            @Nullable final String tokenString = serviceLocator.getTokenService().openReg(login, password);
            return tokenString;
        } catch (EmptyInputException | InvalidPasswordException e) {
            throw new AuthenticationException();
        }
    }

    @Override
    @Nullable
    @WebMethod
    public List<SessionDTO> getListTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getTokenService().validate(tokenString);
        return new Session().getListSessionDTO(serviceLocator.getTokenService().getListSession());
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO getUserByToken(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        return new User().getUserDTO(serviceLocator.getTokenService().getUser(token));
    }

    @Override
    @WebMethod
    public void closeTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        serviceLocator.getTokenService().closeSession(token);
    }

    @Override
    @WebMethod
    public void closeAllTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getTokenService().validate(tokenString);
        serviceLocator.getTokenService().closeAllSession();
    }
}
