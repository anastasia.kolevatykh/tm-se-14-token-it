package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.CommandAdminOnlyException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint() {
    }

    public DomainEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void serialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().serialize();
    }

    @Override
    @WebMethod
    public void deserialize(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().deserialize();
    }

    @Override
    @WebMethod
    public void saveJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().saveJacksonJson();
    }

    @Override
    @WebMethod
    public void loadJacksonJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().loadJacksonJson();
    }

    @Override
    @WebMethod
    public void saveJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().saveJacksonXml();
    }

    @Override
    @WebMethod
    public void loadJacksonXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().loadJacksonXml();
    }

    @Override
    @WebMethod
    public void marshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().marshalJaxbJson();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbJson(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().unmarshalJaxbJson();
    }

    @Override
    @WebMethod
    public void marshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().marshalJaxbXml();
    }

    @Override
    @WebMethod
    public void unmarshalJaxbXml(
            @WebParam(name = "token") @NotNull final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        @Nullable final SessionDTO sessionDTO = token.getSessionDTO();
        if (sessionDTO == null) return;
        if (!RoleType.ADMIN.equals(sessionDTO.getRoleType()))
            throw new CommandAdminOnlyException();
        serviceLocator.getDomainService().unmarshalJaxbXml();
    }
}
