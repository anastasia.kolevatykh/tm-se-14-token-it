package ru.kolevatykh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.exception.*;
import ru.kolevatykh.tm.repository.SessionRepository;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.HibernateUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private IUserService userService;

    public SessionService() {
    }

    public SessionService(@NotNull final IUserService userService) {
        this.setUserService(userService);
    }

    @NotNull
    public IUserService getUserService() {
        return this.userService;
    }

    public void setUserService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public User getUser(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        return user;
    }

    @NotNull
    @Override
    public SessionDTO openAuth(@NotNull final String login, @NotNull final String password)
            throws IOException, UserNotFoundException, InvalidPasswordException, SessionNotFoundException {
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(password)) throw new InvalidPasswordException();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRoleType(user.getRoleType());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO, ConfigUtil.getSalt(), ConfigUtil.getCycle()));
        persist(SessionDTO.getSessionEntity(sessionDTO));
        return sessionDTO;
    }

    @NotNull
    @Override
    public SessionDTO openReg(@NotNull final String login, @NotNull final String password)
            throws IOException, DuplicateUserException, UserNotFoundException, SessionNotFoundException {
        if (userService.findOneByLogin(login) != null) throw new DuplicateUserException();
        @Nullable User user = new User(login, password, RoleType.USER);
        userService.persist(user);
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRoleType(user.getRoleType());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO, ConfigUtil.getSalt(), ConfigUtil.getCycle()));
        persist(SessionDTO.getSessionEntity(sessionDTO));
        return sessionDTO;
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws IOException, CloneNotSupportedException,
            SessionNotFoundException, SessionExpiredException, SignatureCorruptException {
        if (sessionDTO == null) throw new SessionNotFoundException();
        @NotNull final long timeOut = 5 * 60 * 1000L;
        if (System.currentTimeMillis() - sessionDTO.getTimestamp() > timeOut) {
            remove(sessionDTO.getId());
            throw new SessionExpiredException();
        }
        @NotNull final String signature = SessionDTO.generateSignature(sessionDTO);
        if (!signature.equals(sessionDTO.getSignature()))
            throw new SignatureCorruptException("Session");
        if (findOneById(sessionDTO.getId()) == null)
            throw new SessionNotFoundException();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Session> list = new ArrayList<>();
        try {
            list = new SessionRepository(em).findAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    public List<Session> findAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Session> list = new ArrayList<>();
        try {
            list = new SessionRepository(em).findAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws SessionNotFoundException {
        if (id == null || id.isEmpty()) throw new SessionNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable Session session = null;
        try {
            session = new SessionRepository(em).findOneById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return session;
    }

    @Override
    void persist(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new SessionRepository(em).persist(session);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    void merge(@Nullable final Session session) throws SessionNotFoundException {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new SessionRepository(em).merge(session);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String id) throws SessionNotFoundException {
        if (id == null || id.isEmpty()) throw new SessionNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new SessionRepository(em).remove(em.find(Session.class, id));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new SessionRepository(em).removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new SessionRepository(em).removeAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
}
