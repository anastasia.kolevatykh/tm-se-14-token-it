package ru.kolevatykh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.exception.EmptyInputException;
import ru.kolevatykh.tm.exception.ProjectNotFoundException;
import ru.kolevatykh.tm.exception.UserNotFoundException;
import ru.kolevatykh.tm.repository.ProjectRepository;
import ru.kolevatykh.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService {

    public ProjectService() {
    }

    @NotNull
    @Override
    public final List<Project> findAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Project> findAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Nullable
    @Override
    public final Project findOneById(@Nullable final String userId, @Nullable final String id)
            throws EmptyInputException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable Project project = null;
        try {
            project = new ProjectRepository(em).findOneById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return project;
    }

    @NotNull
    @Override
    public final List<Project> findOneByName(@Nullable final String userId, @Nullable final String name)
            throws EmptyInputException, UserNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findOneByName(userId, name);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Override
    public void persist(@Nullable final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new ProjectRepository(em).persist(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new ProjectRepository(em).merge(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id)
            throws EmptyInputException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            @Nullable final Project project = findOneById(userId, id);
            if (project == null) throw new Exception("[The project is not found.]");
            new ProjectRepository(em).remove(project);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new ProjectRepository(em).removeAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new ProjectRepository(em).removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public final List<Project> findAllSortedByCreateDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllSortedByCreateDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Project> findAllSortedByStartDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllSortedByStartDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Project> findAllSortedByFinishDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllSortedByFinishDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Project> findAllSortedByStatus(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllSortedByStatus(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Project> findAllBySearch(@Nullable final String userId, @Nullable String search) throws Exception {
        if (search == null || search.isEmpty()) throw new Exception("[The search query is empty.]");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Project> list = new ArrayList<>();
        try {
            list = new ProjectRepository(em).findAllBySearch(userId, search);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }
}
