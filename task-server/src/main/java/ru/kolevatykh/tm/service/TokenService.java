package ru.kolevatykh.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.dto.SessionDTO;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.exception.*;
import ru.kolevatykh.tm.util.AESUtil;
import ru.kolevatykh.tm.util.ConfigUtil;
import ru.kolevatykh.tm.util.SignatureUtil;

import java.util.List;

public final class TokenService {

    @NotNull
    private ISessionService sessionService;

    public TokenService() {
    }

    public TokenService(@NotNull final ISessionService sessionService) {
        this.setSessionService(sessionService);
    }

    @NotNull
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    public void setSessionService(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void closeAllSession() throws Exception {
        sessionService.removeAll();
    }

    public void closeSession(@NotNull final Token token) throws Exception {
        if (token.getSessionDTO() != null)
            sessionService.remove(token.getSessionDTO().getId());
    }

    @Nullable
    public List<Session> getListSession() throws Exception {
        return sessionService.findAll();
    }

    @Nullable
    public User getUser(@Nullable final Token token) throws Exception {
        if (token == null) throw new SessionNotFoundException();
        @Nullable final User user = sessionService.getUser(token.getSessionDTO());
        return user;
    }

    @NotNull
    public String openAuth(@NotNull final String login, @NotNull final String password) throws Exception {
        return getEncryptedToken(sessionService.openAuth(login, password));
    }

    @NotNull
    public String openReg(@NotNull final String login, @NotNull final String password) throws Exception {
        return getEncryptedToken(sessionService.openReg(login, password));
    }

    @NotNull
    private String getEncryptedToken(@NotNull final SessionDTO sessionDTO) throws Exception {
        @NotNull final Token token = new Token();
        token.setSessionDTO(sessionDTO);
        token.setSignature(SignatureUtil.sign(token, ConfigUtil.getSalt(), ConfigUtil.getCycle()));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonToken = objectMapper.writeValueAsString(token);
        @NotNull final String encryptedToken = new AESUtil().encrypt(jsonToken, ConfigUtil.getKey());
        return encryptedToken;
    }

    @NotNull
    public Token validate(@Nullable final String tokenString) throws Exception {
        if (tokenString == null) throw new SessionNotFoundException();
        @NotNull final String decryptedToken = new AESUtil().decrypt(tokenString, ConfigUtil.getKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Token token = objectMapper.readValue(decryptedToken, Token.class);
        sessionService.validate(token.getSessionDTO());
        @NotNull final String signature = Token.generateSignature(token);
        if (!signature.equals(token.getSignature()))
            throw new SignatureCorruptException("Token");
        return token;
    }
}
