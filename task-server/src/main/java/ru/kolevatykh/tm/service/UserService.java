package ru.kolevatykh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.exception.UserNotFoundException;
import ru.kolevatykh.tm.repository.UserRepository;
import ru.kolevatykh.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService() {
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<User> list = new ArrayList<>();
        try {
            list = new UserRepository(em).findAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable User user = null;
        try {
            user = new UserRepository(em).findOneById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return user;
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) throws UserNotFoundException {
        if (login == null || login.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable User user = null;
        try {
            user = new UserRepository(em).findOneByLogin(login);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return user;
    }

    @Override
    public void persist(@Nullable final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new UserRepository(em).persist(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void merge(@Nullable User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new UserRepository(em).merge(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable String id) throws UserNotFoundException {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new UserRepository(em).remove(em.find(User.class, id));
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new UserRepository(em).removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
}
