package ru.kolevatykh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskService;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.exception.EmptyInputException;
import ru.kolevatykh.tm.exception.TaskNotFoundException;
import ru.kolevatykh.tm.exception.UserNotFoundException;
import ru.kolevatykh.tm.repository.TaskRepository;
import ru.kolevatykh.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public final class TaskService extends AbstractProjectTaskService<Task> implements ITaskService {

    public TaskService() {
    }

    @NotNull
    @Override
    public final List<Task> findAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Nullable
    @Override
    public final Task findOneById(@Nullable final String userId, @Nullable final String id)
            throws UserNotFoundException, EmptyInputException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable Task task = null;
        try {
            task = new TaskRepository(em).findOneById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    public final List<Task> findOneByName(@Nullable final String userId, @Nullable final String name)
            throws UserNotFoundException, EmptyInputException {
        if (name == null || name.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @Nullable List<Task> task = new ArrayList<>();
        try {
            task = new TaskRepository(em).findOneByName(userId, name);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return task;
    }

    @Override
    public void persist(@Nullable final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).persist(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).merge(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id)
            throws UserNotFoundException, EmptyInputException {
        if (id == null || id.isEmpty()) throw new EmptyInputException("task");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            @Nullable final Task task = findOneById(userId, id);
            if (task == null) throw new Exception("[The task is not found.]");
            new TaskRepository(em).remove(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).removeAllByUserId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public final List<Task> findAllSortedByCreateDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllSortedByCreateDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findAllSortedByStartDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllSortedByStartDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findAllSortedByFinishDate(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllSortedByFinishDate(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findAllSortedByStatus(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllSortedByStatus(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findAllBySearch(@Nullable final String userId, @Nullable String search) throws Exception {
        if (search == null || search.isEmpty()) throw new Exception("[The search query is empty.]");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findAllBySearch(userId, search);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @Override
    public void removeTasksWithProjectId(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).removeTasksWithProjectId(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeProjectTasks(@Nullable final String userId, @Nullable final String projectId)
            throws UserNotFoundException, EmptyInputException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            new TaskRepository(em).removeProjectTasks(userId, projectId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public final List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId)
            throws UserNotFoundException, EmptyInputException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyInputException("project");
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findTasksByProjectId(userId, projectId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }

    @NotNull
    @Override
    public final List<Task> findTasksWithoutProject(@Nullable final String userId) throws UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = HibernateUtil.getEntityManager();
        em.getTransaction().begin();
        @NotNull List<Task> list = new ArrayList<>();
        try {
            list = new TaskRepository(em).findTasksWithoutProject(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return list;
    }
}
