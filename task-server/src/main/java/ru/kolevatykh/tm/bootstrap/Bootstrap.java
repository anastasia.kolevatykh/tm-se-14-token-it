package ru.kolevatykh.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.*;
import ru.kolevatykh.tm.constant.WSDLConst;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService);

    @NotNull
    private final TokenService tokenService = new TokenService(sessionService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final TokenEndpoint tokenEndpoint = new TokenEndpoint(this);

    public Bootstrap() {
    }

    public void init() throws Exception {
        Endpoint.publish(WSDLConst.TOKEN_URL, tokenEndpoint);
        Endpoint.publish(WSDLConst.DOMAIN_URL, domainEndpoint);
        Endpoint.publish(WSDLConst.USER_URL, userEndpoint);
        Endpoint.publish(WSDLConst.PROJECT_URL, projectEndpoint);
        Endpoint.publish(WSDLConst.TASK_URL, taskEndpoint);

        System.out.println(WSDLConst.TOKEN_URL);
        System.out.println(WSDLConst.DOMAIN_URL);
        System.out.println(WSDLConst.USER_URL);
        System.out.println(WSDLConst.PROJECT_URL);
        System.out.println(WSDLConst.TASK_URL);

        getDomainService().loadJacksonJson();
    }

    @NotNull
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return this.projectService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return this.taskService;
    }

    @NotNull
    public IDomainService getDomainService() {
        return this.domainService;
    }

    @NotNull
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    @NotNull
    public TokenService getTokenService() {
        return this.tokenService;
    }

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return this.taskEndpoint;
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return this.projectEndpoint;
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return this.domainEndpoint;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return this.userEndpoint;
    }

    @NotNull
    public TokenEndpoint getTokenEndpoint() {
        return this.tokenEndpoint;
    }
}
