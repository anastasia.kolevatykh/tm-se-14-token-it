package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IRepository;
import ru.kolevatykh.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected EntityManager entityManager;

    AbstractRepository(@NotNull final EntityManager entityManager) {
        this.setEntityManager(entityManager);
    }

    public AbstractRepository() {
    }

    @NotNull
    abstract public List<T> findAll() throws Exception;

    @Nullable
    abstract public T findOneById(@NotNull final String id) throws Exception;

    abstract public void persist(@NotNull final T entity) throws Exception;

    abstract public void merge(@NotNull final T entity) throws Exception;

    abstract public void remove(@NotNull final T entity) throws Exception;

    abstract public void removeAll() throws Exception;

    public @NotNull EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
