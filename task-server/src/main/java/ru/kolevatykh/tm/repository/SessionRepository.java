package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionRepository;
import ru.kolevatykh.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public SessionRepository() {
    }

    @NotNull
    @Override
    public List<Session> findAll() throws NoResultException {
        return entityManager
                .createQuery("SELECT s FROM Session s", Session.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAllByUserId(@NotNull String userId) throws NoResultException {
        return entityManager
                .createQuery("SELECT s FROM Session s WHERE s.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) throws NoResultException {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void merge(@NotNull final Session session) {
        entityManager.merge(session);
    }

    @Override
    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    @Override
    public void removeAll() {
        for (@NotNull final Session session : findAll())
            remove(session);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        for (@NotNull final Session session : findAllByUserId(userId))
            remove(session);
    }
}
