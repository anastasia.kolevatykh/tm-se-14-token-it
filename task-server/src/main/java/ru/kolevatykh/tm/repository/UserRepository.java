package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        return entityManager
                .createQuery("SELECT u FROM User u", User.class)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) throws Exception {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public final User findOneByLogin(@NotNull final String login) throws Exception {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.login = :login", User.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void persist(@NotNull final User user) throws Exception {
        entityManager.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) throws Exception {
        entityManager.merge(user);
    }

    @Override
    public void remove(@NotNull final User user) throws Exception {
        entityManager.remove(user);
    }

    @Override
    public void removeAll() throws Exception {
        for (@NotNull final User user : findAll())
            remove(user);
    }
}
