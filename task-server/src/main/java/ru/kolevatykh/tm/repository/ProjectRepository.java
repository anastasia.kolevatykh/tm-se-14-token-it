package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p", Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    public List<Project> findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND p.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public void persist(@NotNull final Project project) throws Exception {
        entityManager.persist(project);
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        entityManager.merge(project);
    }

    @Override
    public void remove(@NotNull final Project project) throws Exception {
        entityManager.remove(project);
    }

    @Override
    public void removeAll() throws Exception {
        for (@NotNull final Project project : findAll())
            remove(project);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        for (@NotNull final Project project : findAllByUserId(userId))
            remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByCreateDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId "
                        + "ORDER BY p.createDate ASC", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStartDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId "
                        + "ORDER BY p.startDate ASC", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByFinishDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId "
                        + "ORDER BY p.finishDate ASC", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId "
                        + "ORDER BY FIELD(p.statusType, 'PLANNED', 'INPROCESS', 'READY')", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId "
                        + "AND (p.name LIKE :search OR p.description LIKE :search)", Project.class)
                .setParameter("userId", userId)
                .setParameter("search", "\"%" + search + "%\"")
                .getResultList();
    }
}
