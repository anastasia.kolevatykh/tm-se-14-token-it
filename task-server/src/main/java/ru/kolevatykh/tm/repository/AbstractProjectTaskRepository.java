package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractProjectTaskRepository<T extends AbstractProjectTaskEntity> {

    @NotNull
    protected EntityManager entityManager;

    AbstractProjectTaskRepository(@NotNull final EntityManager entityManager) {
        this.setEntityManager(entityManager);
    }

    public AbstractProjectTaskRepository() {
    }

    @NotNull
    abstract public List<T> findAll() throws Exception;

    @NotNull
    abstract public List<T> findAllByUserId(@NotNull final String userId) throws Exception;

    @Nullable
    abstract public T findOneById(@NotNull final String userId, @NotNull final String id) throws Exception;

    @NotNull
    abstract public List<T> findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    abstract public void persist(@NotNull final T entity) throws Exception;

    abstract public void merge(@NotNull final T entity) throws Exception;

    abstract public void remove(@NotNull final T entity) throws Exception;

    abstract public void removeAllByUserId(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByCreateDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByStartDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByFinishDate(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllSortedByStatus(@NotNull final String userId) throws Exception;

    @NotNull
    abstract public List<T> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception;

    public @NotNull EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
