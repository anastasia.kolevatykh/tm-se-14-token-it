package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractProjectTaskRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) throws Exception {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t", Task.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    public Task findOneById(@NotNull String id) throws Exception {
        return entityManager.find(Task.class, id);
    }

    @NotNull
    @Override
    public List<Task> findOneByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
    }

    @Override
    public void persist(@NotNull final Task task) throws Exception {
        entityManager.persist(task);
    }

    @Override
    public void merge(@NotNull final Task task) throws Exception {
        entityManager.merge(task);
    }

    @Override
    public void remove(@NotNull final Task task) throws Exception {
        entityManager.remove(task);
    }

    @Override
    public void removeAll() throws Exception {
        for (@NotNull final Task task : findAll())
            remove(task);
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws Exception {
        for (@NotNull final Task task : findAllByUserId(userId))
            remove(task);
    }

    @Override
    public void removeTasksWithProjectId(@NotNull final String userId) throws Exception {
        entityManager
                .createQuery("DELETE FROM Task t WHERE t.user.id = :userId AND t.project IS NOT NULL")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        entityManager
                .createQuery("DELETE FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public final List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public final List<Task> findTasksWithoutProject(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id IS NULL", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByCreateDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.createDate ASC", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStartDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.startDate ASC", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByFinishDate(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.finishDate ASC", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStatus(@NotNull final String userId) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId "
                        + "ORDER BY FIELD(t.statusType, 'PLANNED', 'INPROCESS', 'READY')", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllBySearch(@NotNull final String userId, @NotNull final String search) throws Exception {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId "
                        + "AND (t.name LIKE :search OR t.description LIKE :search)", Task.class)
                .setParameter("userId", userId)
                .setParameter("search", "\"%" + search + "%\"")
                .getResultList();
    }
}
