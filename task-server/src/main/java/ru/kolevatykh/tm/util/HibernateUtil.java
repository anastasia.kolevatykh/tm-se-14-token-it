package ru.kolevatykh.tm.util;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class HibernateUtil {

    @NotNull
    private static EntityManagerFactory entityManagerFactory = factory();

    @SneakyThrows
    public static EntityManagerFactory factory() {
        @NotNull final Properties props = new Properties();
        @NotNull final InputStream is
                = HibernateUtil.class.getResourceAsStream("/META-INF/hibernate.properties");
        props.load(is);
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, props.getProperty("hibernate.driver"));
        settings.put(Environment.URL, props.getProperty("hibernate.url"));
        settings.put(Environment.USER, props.getProperty("hibernate.username"));
        settings.put(Environment.PASS, props.getProperty("hibernate.password"));
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
