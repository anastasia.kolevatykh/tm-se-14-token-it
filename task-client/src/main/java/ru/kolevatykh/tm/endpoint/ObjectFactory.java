
package ru.kolevatykh.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.kolevatykh.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthenticationException_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "AuthenticationException");
    private final static QName _EmptyInputException_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "EmptyInputException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "Exception");
    private final static QName _CloseAllTokenSession_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "closeAllTokenSession");
    private final static QName _CloseAllTokenSessionResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "closeAllTokenSessionResponse");
    private final static QName _CloseTokenSession_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "closeTokenSession");
    private final static QName _CloseTokenSessionResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "closeTokenSessionResponse");
    private final static QName _GetListTokenSession_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "getListTokenSession");
    private final static QName _GetListTokenSessionResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "getListTokenSessionResponse");
    private final static QName _GetUserByToken_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "getUserByToken");
    private final static QName _GetUserByTokenResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "getUserByTokenResponse");
    private final static QName _OpenTokenSessionAuth_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "openTokenSessionAuth");
    private final static QName _OpenTokenSessionAuthResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "openTokenSessionAuthResponse");
    private final static QName _OpenTokenSessionReg_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "openTokenSessionReg");
    private final static QName _OpenTokenSessionRegResponse_QNAME = new QName("http://endpoint.tm.kolevatykh.ru/", "openTokenSessionRegResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kolevatykh.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AuthenticationException }
     * 
     */
    public AuthenticationException createAuthenticationException() {
        return new AuthenticationException();
    }

    /**
     * Create an instance of {@link EmptyInputException }
     * 
     */
    public EmptyInputException createEmptyInputException() {
        return new EmptyInputException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CloseAllTokenSession }
     * 
     */
    public CloseAllTokenSession createCloseAllTokenSession() {
        return new CloseAllTokenSession();
    }

    /**
     * Create an instance of {@link CloseAllTokenSessionResponse }
     * 
     */
    public CloseAllTokenSessionResponse createCloseAllTokenSessionResponse() {
        return new CloseAllTokenSessionResponse();
    }

    /**
     * Create an instance of {@link CloseTokenSession }
     * 
     */
    public CloseTokenSession createCloseTokenSession() {
        return new CloseTokenSession();
    }

    /**
     * Create an instance of {@link CloseTokenSessionResponse }
     * 
     */
    public CloseTokenSessionResponse createCloseTokenSessionResponse() {
        return new CloseTokenSessionResponse();
    }

    /**
     * Create an instance of {@link GetListTokenSession }
     * 
     */
    public GetListTokenSession createGetListTokenSession() {
        return new GetListTokenSession();
    }

    /**
     * Create an instance of {@link GetListTokenSessionResponse }
     * 
     */
    public GetListTokenSessionResponse createGetListTokenSessionResponse() {
        return new GetListTokenSessionResponse();
    }

    /**
     * Create an instance of {@link GetUserByToken }
     * 
     */
    public GetUserByToken createGetUserByToken() {
        return new GetUserByToken();
    }

    /**
     * Create an instance of {@link GetUserByTokenResponse }
     * 
     */
    public GetUserByTokenResponse createGetUserByTokenResponse() {
        return new GetUserByTokenResponse();
    }

    /**
     * Create an instance of {@link OpenTokenSessionAuth }
     * 
     */
    public OpenTokenSessionAuth createOpenTokenSessionAuth() {
        return new OpenTokenSessionAuth();
    }

    /**
     * Create an instance of {@link OpenTokenSessionAuthResponse }
     * 
     */
    public OpenTokenSessionAuthResponse createOpenTokenSessionAuthResponse() {
        return new OpenTokenSessionAuthResponse();
    }

    /**
     * Create an instance of {@link OpenTokenSessionReg }
     * 
     */
    public OpenTokenSessionReg createOpenTokenSessionReg() {
        return new OpenTokenSessionReg();
    }

    /**
     * Create an instance of {@link OpenTokenSessionRegResponse }
     * 
     */
    public OpenTokenSessionRegResponse createOpenTokenSessionRegResponse() {
        return new OpenTokenSessionRegResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "AuthenticationException")
    public JAXBElement<AuthenticationException> createAuthenticationException(AuthenticationException value) {
        return new JAXBElement<AuthenticationException>(_AuthenticationException_QNAME, AuthenticationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyInputException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "EmptyInputException")
    public JAXBElement<EmptyInputException> createEmptyInputException(EmptyInputException value) {
        return new JAXBElement<EmptyInputException>(_EmptyInputException_QNAME, EmptyInputException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseAllTokenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "closeAllTokenSession")
    public JAXBElement<CloseAllTokenSession> createCloseAllTokenSession(CloseAllTokenSession value) {
        return new JAXBElement<CloseAllTokenSession>(_CloseAllTokenSession_QNAME, CloseAllTokenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseAllTokenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "closeAllTokenSessionResponse")
    public JAXBElement<CloseAllTokenSessionResponse> createCloseAllTokenSessionResponse(CloseAllTokenSessionResponse value) {
        return new JAXBElement<CloseAllTokenSessionResponse>(_CloseAllTokenSessionResponse_QNAME, CloseAllTokenSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseTokenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "closeTokenSession")
    public JAXBElement<CloseTokenSession> createCloseTokenSession(CloseTokenSession value) {
        return new JAXBElement<CloseTokenSession>(_CloseTokenSession_QNAME, CloseTokenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseTokenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "closeTokenSessionResponse")
    public JAXBElement<CloseTokenSessionResponse> createCloseTokenSessionResponse(CloseTokenSessionResponse value) {
        return new JAXBElement<CloseTokenSessionResponse>(_CloseTokenSessionResponse_QNAME, CloseTokenSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListTokenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "getListTokenSession")
    public JAXBElement<GetListTokenSession> createGetListTokenSession(GetListTokenSession value) {
        return new JAXBElement<GetListTokenSession>(_GetListTokenSession_QNAME, GetListTokenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListTokenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "getListTokenSessionResponse")
    public JAXBElement<GetListTokenSessionResponse> createGetListTokenSessionResponse(GetListTokenSessionResponse value) {
        return new JAXBElement<GetListTokenSessionResponse>(_GetListTokenSessionResponse_QNAME, GetListTokenSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "getUserByToken")
    public JAXBElement<GetUserByToken> createGetUserByToken(GetUserByToken value) {
        return new JAXBElement<GetUserByToken>(_GetUserByToken_QNAME, GetUserByToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByTokenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "getUserByTokenResponse")
    public JAXBElement<GetUserByTokenResponse> createGetUserByTokenResponse(GetUserByTokenResponse value) {
        return new JAXBElement<GetUserByTokenResponse>(_GetUserByTokenResponse_QNAME, GetUserByTokenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenTokenSessionAuth }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "openTokenSessionAuth")
    public JAXBElement<OpenTokenSessionAuth> createOpenTokenSessionAuth(OpenTokenSessionAuth value) {
        return new JAXBElement<OpenTokenSessionAuth>(_OpenTokenSessionAuth_QNAME, OpenTokenSessionAuth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenTokenSessionAuthResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "openTokenSessionAuthResponse")
    public JAXBElement<OpenTokenSessionAuthResponse> createOpenTokenSessionAuthResponse(OpenTokenSessionAuthResponse value) {
        return new JAXBElement<OpenTokenSessionAuthResponse>(_OpenTokenSessionAuthResponse_QNAME, OpenTokenSessionAuthResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenTokenSessionReg }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "openTokenSessionReg")
    public JAXBElement<OpenTokenSessionReg> createOpenTokenSessionReg(OpenTokenSessionReg value) {
        return new JAXBElement<OpenTokenSessionReg>(_OpenTokenSessionReg_QNAME, OpenTokenSessionReg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenTokenSessionRegResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kolevatykh.ru/", name = "openTokenSessionRegResponse")
    public JAXBElement<OpenTokenSessionRegResponse> createOpenTokenSessionRegResponse(OpenTokenSessionRegResponse value) {
        return new JAXBElement<OpenTokenSessionRegResponse>(_OpenTokenSessionRegResponse_QNAME, OpenTokenSessionRegResponse.class, null, value);
    }

}
