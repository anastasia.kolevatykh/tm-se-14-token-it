package ru.kolevatykh.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.ServiceLocator;

public abstract class AbstractCommand {

    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractCommand(@NotNull final ServiceLocator serviceLocator) {
        this.setServiceLocator(serviceLocator);
    }

    public AbstractCommand() {
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getShortName();

    @NotNull
    public abstract String getDescription();

    public abstract boolean needAuth();

    public abstract void execute() throws Exception;

    @NotNull
    public ServiceLocator getServiceLocator() {
        return this.serviceLocator;
    }

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
