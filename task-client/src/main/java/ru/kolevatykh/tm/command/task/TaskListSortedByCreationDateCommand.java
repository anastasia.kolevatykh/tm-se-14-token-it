package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.TaskDTO;
import ru.kolevatykh.tm.wrapper.TaskWrapper;

import java.util.List;

public final class TaskListSortedByCreationDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list-sorted-by-creation-date";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tlscd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all tasks sorted by the date of creation.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[" + getName().toUpperCase() + "]");
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint().findTasksSortedByCteateDate(token);
        if (taskList == null) {
            throw new Exception("[No tasks yet.]");
        }

        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;
        for (@NotNull final TaskDTO taskDTO : taskList) {
            @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
            tasks
                    .append(++i)
                    .append(". ")
                    .append(taskWrapper.toString())
                    .append(System.lineSeparator());
        }
        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
