package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.UserDTO;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ulo";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLogout from account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[" + getName().toUpperCase() + "]");
        System.out.println("Enter your login to confirm logout: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final String token = serviceLocator.getToken();
        @NotNull final UserDTO userDTO = serviceLocator.getTokenEndpoint().getUserByToken(token);
        if (!userDTO.getLogin().equals(login)) {
            throw new Exception("[The login '" + login + "' is wrong! Enter correct login.]");
        }

        System.out.println("[Confirm logout, y/n: ]");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();
        if (answer.equals("y")) {
            serviceLocator.getTokenEndpoint().closeTokenSession(token);
            serviceLocator.setToken(null);
            System.out.println("[OK]");
        }
    }
}
