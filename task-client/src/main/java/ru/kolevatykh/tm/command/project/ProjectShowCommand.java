package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.constant.Message;
import ru.kolevatykh.tm.endpoint.ProjectDTO;
import ru.kolevatykh.tm.endpoint.TaskDTO;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.wrapper.TaskWrapper;

import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "psh";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[" + getName().toUpperCase() + "]\n" + Message.NAME);
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();
        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().findProjectByName(token, name);
        if (projects == null || projects.isEmpty()) {
            throw new Exception("[The project '" + name + "' does not exist!]");
        }

        @NotNull final String id = projects.get(0).getId();
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint().findTasksByProjectId(token, id);
        if (taskList != null) {
            System.out.println("[TASK LIST]");
            @NotNull final StringBuilder tasks = new StringBuilder();
            int i = 0;

            for (@NotNull final TaskDTO taskDTO : taskList) {
                if (id.equals(taskDTO.getProjectId())) {
                    @NotNull final TaskWrapper taskWrapper = new TaskWrapper(taskDTO);
                    tasks
                            .append(++i)
                            .append(". ")
                            .append(taskWrapper.toString())
                            .append(System.lineSeparator());
                }
            }
            @NotNull final String taskString = tasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
