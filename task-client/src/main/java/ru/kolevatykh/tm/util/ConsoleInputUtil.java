package ru.kolevatykh.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class ConsoleInputUtil {

    @NotNull
    private static Scanner consoleInput = new Scanner(System.in);

    public ConsoleInputUtil() {
    }

    @NotNull
    public static String getConsoleInput() {
        return consoleInput.nextLine();
    }
}
