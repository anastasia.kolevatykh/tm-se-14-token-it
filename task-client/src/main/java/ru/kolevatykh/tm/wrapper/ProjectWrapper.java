package ru.kolevatykh.tm.wrapper;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.endpoint.ProjectDTO;

public class ProjectWrapper extends ProjectDTO {

    @NotNull
    private ProjectDTO project;

    public ProjectWrapper(@NotNull final ProjectDTO project) {
        this.project = project;
    }

    public ProjectWrapper() {
    }

    @Override
    public String toString() {
        return "userId: '" + project.getUserId() + '\'' +
                ", id: '" + project.getId() + '\'' +
                ", name: '" + project.getName() + '\'' +
                ", description: '" + project.getDescription() + '\'' +
                ", statusType: " + project.getStatusType() +
                ", startDate: " + project.getStartDate() +
                ", finishDate: " + project.getFinishDate();
    }

    @NotNull
    public ProjectDTO getProject() {
        return this.project;
    }

    public void setProject(@NotNull final ProjectDTO project) {
        this.project = project;
    }
}
