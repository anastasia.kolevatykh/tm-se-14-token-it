package ru.kolevatykh.tm.it;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.constant.WSDLConst;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class ProjectEndpointServiceIT {

    @NotNull
    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();

    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpointService(new URL(WSDLConst.PROJECT_URL)).getProjectEndpointPort();

    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpointService(new URL(WSDLConst.TASK_URL)).getTaskEndpointPort();

    @Nullable
    private String token;

    ProjectEndpointServiceIT() throws MalformedURLException {
    }

    @BeforeEach
    void setUp() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final String login = "testTaskUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTaskUser");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
        projectEndpoint.persistProject(token, "demo project", "", "1.1.2019", "1.1.2020");
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void findProjectsSortedByStatus() throws Exception_Exception {
        projectEndpoint.persistProject(token,"demo project 2", "", "1.1.2009", "1.1.2010");
        @NotNull final List<ProjectDTO> tempProject = projectEndpoint.findProjectByName(token, "demo project 2");
        projectEndpoint.mergeProjectStatus(token, tempProject.get(0).getId(), "READY");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectsSortedByStatus(token);
        @NotNull int prevStatus = 0;
        for (@NotNull final ProjectDTO projectDTO : projects) {
            @NotNull int currStatus = projectDTO.getStatusType().ordinal();
            assertFalse(prevStatus > currStatus);
            prevStatus = currStatus;
        }
    }

    @Test
    void mergeProjectStatus() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        projectEndpoint.mergeProjectStatus(token, projects.get(0).getId(), "READY");
        @NotNull final ProjectDTO tempProject = projectEndpoint.findProjectById(token, projects.get(0).getId());
        assertNotEquals(projects.get(0).getStatusType().value(), tempProject.getStatusType().value());
    }

    @Test
    void findAllProjectsByUserId() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 2", "", "12.12.2012", "12.12.2020");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertFalse(projects.isEmpty());
        assertEquals(2, projects.size());
    }

    @Test
    void findProjectsSortedByFinishDate() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2015", "1.1.2017");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectsSortedByFinishDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getFinishDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getFinishDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void removeProject() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projectsByName = projectEndpoint.findProjectByName(token, "demo project");
        projectEndpoint.removeProject(token, projectsByName.get(0).getId());
        @NotNull final List<ProjectDTO> projectsByUserId = projectEndpoint.findAllProjectsByUserId(token);
        for (@NotNull ProjectDTO project : projectsByUserId)
            assertNotEquals("demo project", project.getName());
    }

    @Test
    void persistProject() throws Exception_Exception {
        projectEndpoint.persistProject(token, "demo project 2", "desc", "1.1.2019", "1.1.2020");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(token);
        for (@NotNull final ProjectDTO project : projects)
            if (project.getName().equals("demo project 2"))
                assertEquals("demo project 2", project.getName());
        assertFalse(projects.isEmpty());
    }

    @Test
    void findProjectByName() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 3", "", "3.3.2020", "12.12.2025");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "mega project 3");
        assertNotNull(projects.get(0));
    }

    @Test
    void findProjectById() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        assertNotNull(projectEndpoint.findProjectById(token, projects.get(0).getId()));
    }

    @Test
    void findProjectsSortedByStartDate() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 4", "", "1.1.2012", "1.1.2017");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectsSortedByStartDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getStartDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getStartDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void findProjectsBySearch() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mini project 2", "", "1.1.2009", "1.1.2010");
        projectEndpoint.persistProject(token, "super project 3", "desc mini", "1.1.2013", "1.1.2016");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectsBySearch(token, "mini");
        for (@NotNull final ProjectDTO project : projects)
            assertTrue(project.getName().contains("mini") | project.getDescription().contains("mini"));
    }

    @Test
    void mergeProject() throws Exception_Exception {
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectByName(token, "demo project");
        projectEndpoint.mergeProject(token, projects.get(0).getId(), "super project",
                projects.get(0).getDescription(), "1.1.2014", "1.1.2023");
        @NotNull final List<ProjectDTO> tempProject = projectEndpoint.findProjectByName(token, "super project");
        assertNotEquals(projects.get(0).getName(), tempProject.get(0).getName());
    }

    @Test
    void findProjectsSortedByCreateDate() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2018", "1.1.2022");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findProjectsSortedByCreateDate(token);
        @NotNull XMLGregorianCalendar prevTime = projects.get(0).getCreateDate();
        for (int i = 1; i < projects.size(); i++) {
            @NotNull XMLGregorianCalendar currTime = projects.get(i).getCreateDate();
            assertFalse(prevTime.compare(currTime) > 0);
            prevTime = currTime;
        }
    }

    @Test
    void removeAllProjects() throws Exception_Exception {
        projectEndpoint.persistProject(token, "mega project 2", "", "1.1.2018", "1.1.2022");
        @NotNull final List<ProjectDTO> projectsByName = projectEndpoint.findProjectByName(token,"mega project 2");
        assertNotNull(taskEndpoint);
        taskEndpoint.persistTask(token, projectsByName.get(0).getId(), "mega task 2", "", "1.1.2009", "1.1.2010");
        taskEndpoint.persistTask(token, projectsByName.get(0).getId(), "mini task 3", "", "1.1.2011", "1.1.2013");
        projectEndpoint.removeAllProjects(token);
        @NotNull final List<ProjectDTO> projectsByUserId = projectEndpoint.findAllProjectsByUserId(token);
        assertTrue(projectsByUserId.isEmpty());
    }
}