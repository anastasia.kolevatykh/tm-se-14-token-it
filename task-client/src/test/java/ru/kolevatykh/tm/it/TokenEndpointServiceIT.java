package ru.kolevatykh.tm.it;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.constant.WSDLConst;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.endpoint.Exception;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class TokenEndpointServiceIT {

    @NotNull
    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();

    @Nullable
    private String token = null;

    TokenEndpointServiceIT() throws MalformedURLException {
    }

    @BeforeEach
    void setUp() throws Exception_Exception, MalformedURLException, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final String login = "testToken";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testToken");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void getUserAuthException() {
        @NotNull final String login = "admin0";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass");
        Throwable exception = assertThrows(ServerSOAPFaultException.class, () -> {
            tokenEndpoint.openTokenSessionAuth(login, pass);
        });
        String expectedMessage = "[The user doesn't exist.]";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void getUserByToken() throws Exception_Exception {
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        assertNotNull(user);
    }

    @Test
    void openTokenSessionAuth() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        userEndpoint.removeUser(token);
        @NotNull final String login = "admin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("pass1");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        assertNotNull(token);
    }

    @Test
    void closeAllTokenSession() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        tokenEndpoint.closeAllTokenSession(token);
        @NotNull final String login = "testToken";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testToken");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        @NotNull final List<SessionDTO> sessions = tokenEndpoint.getListTokenSession(token);
        for (@NotNull final SessionDTO session : sessions)
            assertEquals(session.getUserId(), user.getId());
    }

    @Test
    void closeTokenSession() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        tokenEndpoint.closeTokenSession(token);
        Throwable exception = assertThrows(ServerSOAPFaultException.class, () -> {
            tokenEndpoint.getUserByToken(token);
        });
        String expectedMessage = "[The session does not exist.]";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        @NotNull final String login = "testToken";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testToken");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
    }

    @Test
    void getListTokenSession() throws Exception_Exception {
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        @NotNull final List<SessionDTO> sessions = tokenEndpoint.getListTokenSession(token);
        for (@NotNull final SessionDTO session : sessions)
            if (session.getUserId().equals(user.getId()))
                assertEquals(session.getUserId(), user.getId());
        assertFalse(sessions.isEmpty());
    }

    @Test
    void openTokenSessionReg() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        userEndpoint.removeUser(token);
        token = null;
        @NotNull final String login = "testTokenUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testTokenUser");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
    }
}