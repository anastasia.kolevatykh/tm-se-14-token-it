package ru.kolevatykh.tm.it;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.constant.WSDLConst;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.junit.experimental.categories.Category;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class UserEndpointServiceIT {

    @NotNull
    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();

    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpointService(new URL(WSDLConst.DOMAIN_URL)).getDomainEndpointPort();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();

    @Nullable
    private String token;

    UserEndpointServiceIT() throws MalformedURLException {
    }

    @BeforeEach
    void setUp() throws Exception_Exception, MalformedURLException, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void tryUserAuthTest() throws Exception_Exception {
        @NotNull final String login = "testUser1";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser1");
        Throwable exception = assertThrows(ServerSOAPFaultException.class, () -> {
            tokenEndpoint.openTokenSessionAuth(login, pass);
        });
        String expectedMessage = "[The user doesn't exist.]";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        @NotNull final UserDTO currentUser = userEndpoint.findUserById(token);
        assertNotNull(currentUser);
    }

    @Test
    void findAllUsersTest() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, user.getLogin(), "testUser",
                PasswordHashUtil.getPasswordHash("testUser"), "ADMIN");
        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        @NotNull final List<UserDTO> userList = userEndpoint.findAllUsers(token);
        assertFalse(userList.isEmpty() & userList.size() != 3);
    }

    @Test
    void getUserByIdTest() throws Exception_Exception {
        @NotNull final UserDTO user = userEndpoint.findUserById(token);
        assertNotNull(user);
    }

    @Test
    void editUserTest() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, user.getLogin(), "testUser",
                PasswordHashUtil.getPasswordHash("testUser"), "ADMIN");
        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");
        @NotNull final String tokenNew = tokenEndpoint.openTokenSessionAuth(login, pass);
        assertNotEquals(tokenNew, token);
        token = tokenNew;
    }

    @Test
    void removeUserTest() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        userEndpoint.removeUser(token);
        token = null;
        @NotNull final String login = "testUser";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUser");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        @NotNull final UserDTO user = tokenEndpoint.getUserByToken(token);
        @NotNull final String loginNew = "testUserAdmin";
        @NotNull final String passNew = PasswordHashUtil.getPasswordHash("testUserAdmin");
        userEndpoint.mergeUser(token, user.getLogin(), loginNew,
                passNew, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(token);
        for (@NotNull final UserDTO tempUser : users) {
            assertNotEquals("testUser", tempUser.getLogin());
        }
    }

    @Test
    void removeAllUsers() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        tokenEndpoint.closeTokenSession(token);
        @NotNull final String login = "testUserAdmin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testUserAdmin");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        @NotNull UserDTO currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), login, pass, "ADMIN");

        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        userEndpoint.removeAllUsers(token);
        token = null;

        token = tokenEndpoint.openTokenSessionReg(login, pass);
        @NotNull final String loginNew = "tempUserAdmin";
        @NotNull final String passNew = PasswordHashUtil.getPasswordHash("tempUserAdmin");
        currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), loginNew, passNew, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);

        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(token);
        for (@NotNull final UserDTO user : users)
            assertEquals("tempUserAdmin", user.getLogin());

        domainEndpoint.loadJacksonJson(token);
    }
}