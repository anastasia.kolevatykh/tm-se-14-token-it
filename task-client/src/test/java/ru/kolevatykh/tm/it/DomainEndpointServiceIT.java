package ru.kolevatykh.tm.it;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.kolevatykh.tm.constant.WSDLConst;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Category(IntegrationTest.class)
@Tag("integration-test")
class DomainEndpointServiceIT {

    @NotNull
    private ITokenEndpoint tokenEndpoint = new TokenEndpointService(new URL(WSDLConst.TOKEN_URL)).getTokenEndpointPort();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService(new URL(WSDLConst.USER_URL)).getUserEndpointPort();

    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpointService(new URL(WSDLConst.DOMAIN_URL)).getDomainEndpointPort();

    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpointService(new URL(WSDLConst.PROJECT_URL)).getProjectEndpointPort();

    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpointService(new URL(WSDLConst.TASK_URL)).getTaskEndpointPort();

    @Nullable
    private String token = null;

    DomainEndpointServiceIT() throws MalformedURLException {
    }

    @BeforeEach
    void setUp() throws AuthenticationException_Exception, EmptyInputException_Exception, Exception_Exception, MalformedURLException {
        @NotNull final String login = "testDomainAdmin";
        @NotNull final String pass = PasswordHashUtil.getPasswordHash("testDomainAdmin");
        token = tokenEndpoint.openTokenSessionReg(login, pass);
        assertNotNull(token);
        @NotNull UserDTO currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), login, pass, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(login, pass);
        assertNotNull(token);
    }

    @AfterEach
    void tearDown() throws Exception_Exception {
        assertNotNull(userEndpoint);
        userEndpoint.removeUser(token);
        token = null;
    }

    @Test
    void save_loadJacksonJson() throws AuthenticationException_Exception, EmptyInputException_Exception, Exception_Exception {
        domainEndpoint.saveJacksonJson(token);
        userEndpoint.removeAllUsers(token);
        token = null;
        @NotNull final String loginNew = "testDomainAdmin";
        @NotNull final String passNew = PasswordHashUtil.getPasswordHash("testDomainAdmin");
        token = tokenEndpoint.openTokenSessionReg(loginNew, passNew);
        @NotNull final UserDTO currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), loginNew, passNew, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(token);
        for (@NotNull final UserDTO user : users)
            assertEquals("testDomainAdmin", user.getLogin());
        domainEndpoint.loadJacksonJson(token);
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth("admin", PasswordHashUtil.getPasswordHash("pass1"));
        @NotNull final List<UserDTO> tempUsers = userEndpoint.findAllUsers(token);
        assertEquals(3, tempUsers.size());
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertEquals(3, projects.size());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertEquals(2, tasks.size());
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
    }

    @Test
    void serialize_deserialize() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        domainEndpoint.serialize(token);
        userEndpoint.removeAllUsers(token);
        token = null;
        @NotNull final String loginNew = "testDomainAdmin";
        @NotNull final String passNew = PasswordHashUtil.getPasswordHash("testDomainAdmin");
        token = tokenEndpoint.openTokenSessionReg(loginNew, passNew);
        @NotNull final UserDTO currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), loginNew, passNew, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(token);
        for (@NotNull final UserDTO user : users)
            assertEquals("testDomainAdmin", user.getLogin());
        domainEndpoint.deserialize(token);
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth("admin", PasswordHashUtil.getPasswordHash("pass1"));
        @NotNull final List<UserDTO> tempUsers = userEndpoint.findAllUsers(token);
        assertEquals(3, tempUsers.size());
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertEquals(3, projects.size());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertEquals(2, tasks.size());
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
    }

    @Test
    void save_loadJacksonXml() throws Exception_Exception, EmptyInputException_Exception, AuthenticationException_Exception {
        domainEndpoint.saveJacksonXml(token);
        userEndpoint.removeAllUsers(token);
        token = null;
        @NotNull final String loginNew = "testDomainAdmin";
        @NotNull final String passNew = PasswordHashUtil.getPasswordHash("testDomainAdmin");
        token = tokenEndpoint.openTokenSessionReg(loginNew, passNew);
        @NotNull final UserDTO currentUser = tokenEndpoint.getUserByToken(token);
        userEndpoint.mergeUser(token, currentUser.getLogin(), loginNew, passNew, "ADMIN");
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(token);
        for (@NotNull final UserDTO user : users)
            assertEquals("testDomainAdmin", user.getLogin());
        domainEndpoint.loadJacksonXml(token);
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth("admin", PasswordHashUtil.getPasswordHash("pass1"));
        @NotNull final List<UserDTO> tempUsers = userEndpoint.findAllUsers(token);
        assertEquals(3, tempUsers.size());
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(token);
        assertEquals(3, projects.size());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(token);
        assertEquals(2, tasks.size());
        tokenEndpoint.closeTokenSession(token);
        token = tokenEndpoint.openTokenSessionAuth(loginNew, passNew);
    }
}