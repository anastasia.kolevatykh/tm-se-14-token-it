# Task Manager

### Описание

Приложение для создания списков задач.

### Требования к software:
1. Java 1.8

### Технологический стек:
1. Java 1.8
2. Maven 3.8.0

### Имя и контакты разработчика

Anastasia Kolevatykh

anastasia.kolevatykh@gmail.com

### Команды для сборки приложения:
```
mvn clean install
```
### Команды для запуска приложения:
```
java -jar target/task-manager-1.0.0.jar
```

### Команда для проверки прохождения интеграционных тестов:
```
java -version (expected = 1.8)

Установка версии 1.8:
export JAVA_HOME=$(/usr/libexec/java_home -v1.8)

При запущенном сервере запустить тесты:
mvn test -Pintegration-test-latest
```